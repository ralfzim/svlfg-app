
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:svlfg/camera.dart';
import 'package:svlfg/portal.dart';
import 'package:url_launcher/url_launcher.dart';

void main() async{

  WidgetsFlutterBinding.ensureInitialized();

  final cameras = await availableCameras();
  final firstCamera = cameras.first;

  runApp(MaterialApp(
    title: 'SVLFG',
    theme: ThemeData(
        primarySwatch: Colors.grey
    ),
    home: MyHomePage(title: 'SVLFG', camera: firstCamera),
  ));
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title, required this.camera,});

  final String title;
  final CameraDescription camera;
  @override
  State<MyHomePage> createState() => _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(242, 245, 247, 1),
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.lightGreen, weight: 400.0, size: 30),
      ),
      body: GridView(
        padding: const EdgeInsets.all(15),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: [
          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => PortalState().build(context))),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Card(
                elevation: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/keimling-512.png", height: 80, width: 80),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text("Zum SVLFG Portal", style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Camera().build(context), settings: RouteSettings(arguments: widget.camera))),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Card(
                elevation: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/kamera.png", height: 80, width: 80),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text("Kamera öffnen", style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
              onTap: _launchURL,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Card(
                elevation: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/Digi_Frage_Antwort_rgb.png", height: 80, width: 80),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text("Zur Stockfibel App", style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),

          GestureDetector(
            onTap: _launchTel,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Card(
                  elevation: 4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/telefon.PNG", height: 80, width: 80),
                        ],
                      ),
                      const SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text("1234 anrufen", style: TextStyle(fontSize: 12)),
                        ],
                      ),
                    ],
                  ),
                ),
            )
          ),
        ],
      ),
      drawer: _MainDrawer(),
    );
  }

  _launchURL() async{
    final appId = Platform.isAndroid ? 'io.gruenecho.stockfibel_to_go&gl=de' : 'YOUR_IOS_APP_ID';
    final url = Uri.parse(
      Platform.isAndroid
          ? "market://details?id=$appId"
          : "https://apps.apple.com/app/id$appId",
    );
    await launchUrl(url, mode: LaunchMode.externalApplication);
  }

  _launchTel() async{
    final appId = Platform.isAndroid ? 'io.gruenecho.stockfibel_to_go&gl=de' : 'YOUR_IOS_APP_ID';
    final url = Uri.parse(
      Platform.isAndroid
          ? "tel://1234"
          : "https://apps.apple.com/app/id$appId",
    );
    await launchUrl(url, mode: LaunchMode.externalApplication);
  }

}

class _MainDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          const SizedBox(
            height: 65,
            child:DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.lightGreen,
              ),
              child: Text("Menü", style: TextStyle(color: Colors.white, fontSize: 25)),
            ) ,
          ),
          ListTile(
            leading: const Icon(Icons.input),
            title: const Text('Datenschutz'),
            onTap: () => {},
          ),
        ],
      ),
    );
  }

}
