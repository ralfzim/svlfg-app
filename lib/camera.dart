import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:svlfg/main.dart';



class Camera extends State<MyHomePage>{
  late final CameraController _controller;
  late final Future<void> _initializeControllerFuture;

  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final camera = ModalRoute.of(context)!.settings.arguments as CameraDescription;
    _controller = CameraController(camera, ResolutionPreset.medium);
    _initializeControllerFuture = _controller.initialize();

    return Scaffold(
      backgroundColor: const Color.fromRGBO(242, 245, 247, 1),
      appBar: AppBar(
        title: const Text("SVLFG"),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.lightGreen, weight: 400.0, size: 30),
      ),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done){
            return CameraPreview(_controller);
          }else{
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightGreen,
        onPressed: () async {
          try{
            await _initializeControllerFuture;
            
            final image = await _controller.takePicture();
            
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => DisplayPictureScreen(imagePath: image.path))
            );
          } catch(e) {
            if (kDebugMode) {
              if (kDebugMode) {
                print(e);
              }
            }
          }
        },
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({super.key, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(242, 245, 247, 1),
      appBar: AppBar(
          title: const Text('Display the Picture'),
        backgroundColor: Colors.white,
      ),

      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
    );
  }
}