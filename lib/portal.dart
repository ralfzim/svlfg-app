import 'package:flutter/material.dart';
import 'package:svlfg/main.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PortalState extends State<MyHomePage>{
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: const Text("SVLFG"),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.lightGreen, weight: 400.0, size: 30),
      ),
      body: WebViewWidget(controller: 
      WebViewController()
        ..setJavaScriptMode(JavaScriptMode.unrestricted)
        ..setBackgroundColor(const Color(0x00000000))
        ..setNavigationDelegate(
            NavigationDelegate(
            onProgress: (int progress){},
            onPageStarted: (String url){},
            onPageFinished: (String url){},
            onWebResourceError: (WebResourceError error){},
            onNavigationRequest: (NavigationRequest request) {
              return NavigationDecision.navigate;
            }
          )
        )
        ..loadRequest(Uri.parse("https://portal.svlfg.de"))
      ),
    );
  }

}